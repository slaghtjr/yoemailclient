﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YoEmailClient.Startup))]
namespace YoEmailClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
