﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YoEmailClient.Models
{
    public class Email
    {
        [DatabaseGenerated(databaseGeneratedOption:DatabaseGeneratedOption.Identity)]
        public int EmailID { get; set; }

        [Required]
        public string Sender { get; set; }

        [Required]
        public virtual ICollection <string> Reciever { get; set; }

        [Required]
        public string SubjectLine { get; set; }

        [Required]
        public string Body { get; set; }

        [Required]
        public DateTime DateTimeSent { get; set; }

        [Required]
        public Boolean IsDraft { get; set; }
    }
}